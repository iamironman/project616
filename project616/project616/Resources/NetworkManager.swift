//
//  NetworkManager.swift
//  project616
//
//  Created by Adrian Eves on 5/22/22.
//

import Foundation
import CryptoKit
import CommonCrypto

// The purpose of this file is to run the network details
// for the API in order to abstract logic from our view models.

class NetworkManager: ObservableObject {
    @Published var results = [Comic]()
    @Published var state: NetworkContentAvailable = .loading
    
    var url: URL
    
    init(url: URL) {
        self.url = url
        loadComicCollectionTest() {}
    }
    
    convenience init() {
        self.init(url: URL(string: MarvelURL.urlString())!)
    }
    
    func loadComicCollectionTest(using session: URLSessionProtocol = URLSession.shared, completionHandler: @escaping () -> Void) {
        
        let task = session.dataTask(with: url) {(data, response, error) in
            if let data = data {
                do {
                    let decodedResponse = try JSONDecoder().decode(ComicDataWrapper.self, from: data)
                    DispatchQueue.main.async {
                        self.results = decodedResponse.data?.results ?? []
                        self.state = .available
                    }
                } catch {
                    print(error)
                    self.state = .unavailable
                }
            }
            
            completionHandler()
        }
        task.resume()
    }
}

struct NetworkConfig {
    static var configuration: URLSessionConfiguration {
        let config = URLSessionConfiguration.default
        config.waitsForConnectivity = true
        config.timeoutIntervalForRequest = 10
        config.timeoutIntervalForResource = 10
        
        return config
    }
}

struct MarvelURL {
    static func urlString() -> String {
        // Hey reviewers: I'd love to talk about the next line of code.
        // Using Date() by itself gave me API problems, claiming my
        // hash was invalid, yet decrementing it by 1 allowed me
        // to read the list of comics. It may be that I'm missing
        // some crucial detail in this construction or it's just
        // some bug, but either way, I'd love to hear your thoughts on
        // the matter!
        let timestamp = Date().advanced(by: -1)
        
        // If I can't get either key, I'm going to return an empty string
        // just to intentionally make the url fail
        guard let publicKey = Bundle.main.infoDictionary?["PUBLIC_KEY"] as? String else {
            return ""
        }
        
        guard let privateKey = Bundle.main.infoDictionary?["PRIVATE_KEY"] as? String else {
            return ""
        }
        let preHashString = "\(timestamp.timeIntervalSince1970)\(privateKey)\(publicKey)"
        let hashed = preHashString.md5
           
        return "https://gateway.marvel.com:443/v1/public/comics?ts=\(timestamp.timeIntervalSince1970)&format=comic&noVariants=true&dateDescriptor=lastWeek&limit=20&apikey=6a996e71627c4a7779376cbeb2d9f88a&hash=\(hashed)"
    }
}

extension String {
    var md5: String {
        let data = Data(self.utf8)
        let hash = data.withUnsafeBytes { (bytes: UnsafeRawBufferPointer) -> [UInt8] in
            var hash = [UInt8](repeating: 0, count: Int(CC_MD5_DIGEST_LENGTH))
            CC_MD5(bytes.baseAddress, CC_LONG(data.count), &hash)
            return hash
        }
        return hash.map { String(format: "%02x", $0) }.joined()
    }
}
