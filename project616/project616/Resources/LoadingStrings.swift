//
//  LoadingStrings.swift
//  project616
//
//  Created by Adrian Eves on 5/22/22.
//

import Foundation

class LoadingString {
    static let quotes = "Polishing Cap's Shield..., Lifting Mjolnir..., Checking the Sanctum..., Dialing the Baxter Building..., Changing out web cartridges..., Checking with The Collector..., Analyzing Gamma exposure..., Removing extra symbiotes..., Untangling the Multiverse..., Scanning for undevoured planets..., Cloaking from Sentinels initiated..."
    static func randomString() -> String {
        let arr = quotes.components(separatedBy: ", ")
        return arr.randomElement() ?? "Loading..."
    }
}
