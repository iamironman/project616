//
//  project616App.swift
//  project616
//
//  Created by Adrian Eves on 5/22/22.
//

import SwiftUI

@main
struct project616App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
