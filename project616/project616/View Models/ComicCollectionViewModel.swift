//
//  ComicCollectionViewModel.swift
//  project616
//
//  Created by Adrian Eves on 5/22/22.
//

import Combine
import Foundation

enum NetworkContentAvailable {
    case loading
    case available
    case unavailable
}

class ComicCollectionViewModel: ObservableObject {
    @Published var selectedComic: Comic?
    @Published var contentStatus: NetworkContentAvailable = .loading
    @Published var networkManager = NetworkManager()
    var subscriptions = Set<AnyCancellable>()
    
    init() {
        networkManager.$state.sink { [weak self] newValue in
            self?.contentStatus = newValue
        }.store(in: &subscriptions)
    }
    
    func newComicSelect(comic: Comic) {
        selectedComic = comic
    }
}
