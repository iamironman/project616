//
//  ContentView.swift
//  project616
//
//  Created by Adrian Eves on 5/22/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        ComicCollectionListView()
    }
}

//struct ContentView_Previews: PreviewProvider {
//    static var previews: some View {
//        ContentView()
//    }
//}
