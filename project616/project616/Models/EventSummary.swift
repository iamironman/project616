//
//  EventSummary.swift
//  project616
//
//  Created by Adrian Eves on 5/22/22.
//

import Foundation

struct EventSummary: Codable {
    var resourceURI: String? // resourceURI (string, optional): The path to the individual event resource.,
    var name: String? // name (string, optional): The name of the event.
}
