//
//  ExternalImage.swift
//  project616
//
//  Created by Adrian Eves on 5/22/22.
//

import Foundation

struct ExternalImage: Codable {
    var path: String? // path (string, optional): The directory path of to the image.,
    var `extension`: String? // extension (string, optional): The file extension for the image.
}
