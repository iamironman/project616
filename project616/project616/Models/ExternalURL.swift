//
//  ExternalURL.swift
//  project616
//
//  Created by Adrian Eves on 5/22/22.
//

import Foundation

struct ExternalURL: Codable {
    var type: String? // type (string, optional): A text identifier for the URL.,
    var url: String? // url (string, optional): A full URL (including scheme, domain, and path).
}
