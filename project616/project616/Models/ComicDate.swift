//
//  ComicDate.swift
//  project616
//
//  Created by Adrian Eves on 5/22/22.
//

import Foundation

struct ComicDate: Codable {
    var type: String? // type (string, optional): A description of the date (e.g. onsale date, FOC date).,
    var date: String? // date (Date, optional): The date.
}
