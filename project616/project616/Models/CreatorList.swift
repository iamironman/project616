//
//  CreatorList.swift
//  project616
//
//  Created by Adrian Eves on 5/22/22.
//

import Foundation

struct CreatorList: Codable {
    var available: Int? // available (int, optional): The number of total available creators in this list. Will always be greater than or equal to the "returned" value.,
    var returned: Int? // returned (int, optional): The number of creators returned in this collection (up to 20).,
    var collectionURI: String? // collectionURI (string, optional): The path to the full list of creators in this collection.,
    // TODO: CreatorSummary
    var items: [CreatorSummary]? // items (Array[CreatorSummary], optional): The list of returned creators in this collection.
}
