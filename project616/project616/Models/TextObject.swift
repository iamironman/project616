//
//  TextObject.swift
//  project616
//
//  Created by Adrian Eves on 5/22/22.
//

import Foundation

struct TextObject: Codable {
    var type: String? //type (string, optional): The canonical type of the text object (e.g. solicit text, preview text, etc.).,
    var language: String? //language (string, optional): The IETF language tag denoting the language the text object is written in.,
    var text: String? //text (string, optional): The text.
}
