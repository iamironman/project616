//
//  StoryList.swift
//  project616
//
//  Created by Adrian Eves on 5/22/22.
//

import Foundation

struct StoryList: Codable {
    var available: Int? // available (int, optional): The number of total available stories in this list. Will always be greater than or equal to the "returned" value.,
    var returned: Int? // returned (int, optional): The number of stories returned in this collection (up to 20).,
    var collectionURI: String? // collectionURI (string, optional): The path to the full list of stories in this collection.,
    var items: [StorySummary]? // items (Array[StorySummary], optional): The list of returned stories in this collection.
}
