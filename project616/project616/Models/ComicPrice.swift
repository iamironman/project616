//
//  ComicPrice.swift
//  project616
//
//  Created by Adrian Eves on 5/22/22.
//

import Foundation

struct ComicPrice: Codable {
    var type: String? // type (string, optional): A description of the price (e.g. print price, digital price).,
    var price: Float? // price (float, optional): The price (all prices in USD).
}
