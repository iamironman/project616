//
//  CharacterSummary.swift
//  project616
//
//  Created by Adrian Eves on 5/22/22.
//

import Foundation

struct CharacterSummary: Codable {
    var resourceURI: String? // resourceURI (string, optional): The path to the individual creator resource.,
    var name: String? // name (string, optional): The full name of the creator.,
    var role: String? // role (string, optional): The role of the creator in the parent entity.
}
