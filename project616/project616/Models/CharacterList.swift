//
//  CharacterList.swift
//  project616
//
//  Created by Adrian Eves on 5/22/22.
//

import Foundation

struct CharacterList: Codable {
    var available: Int? // available (int, optional): The number of total available characters in this list. Will always be greater than or equal to the "returned" value.,
    var returned: Int? // returned (int, optional): The number of characters returned in this collection (up to 20).,
    var collectionURI: String? // collectionURI (string, optional): The path to the full list of characters in this collection.,
    var items: [CharacterSummary]? // items (Array[CharacterSummary], optional): The list of returned characters in this collection.
}
