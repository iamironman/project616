//
//  EventList.swift
//  project616
//
//  Created by Adrian Eves on 5/22/22.
//

import Foundation

struct EventList: Codable {
    var available: Int? // available (int, optional): The number of total available events in this list. Will always be greater than or equal to the "returned" value.,
    var returned: Int? // returned (int, optional): The number of events returned in this collection (up to 20).,
    var collectionURI: String? // collectionURI (string, optional): The path to the full list of events in this collection.,
    var items: [EventSummary]? // items (Array[EventSummary], optional): The list of returned events in this collection.
}
