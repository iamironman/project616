//
//  SeriesSummary.swift
//  project616
//
//  Created by Adrian Eves on 5/22/22.
//

import Foundation

struct SeriesSummary: Codable {
    var resourceURI: String? // resourceURI (string, optional): The path to the individual series resource.,
    var name: String? // name (string, optional): The canonical name of the series.
}
