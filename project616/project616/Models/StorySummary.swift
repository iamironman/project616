//
//  StorySummary.swift
//  project616
//
//  Created by Adrian Eves on 5/22/22.
//

import Foundation

struct StorySummary: Codable {
    var resourceURI: String? // resourceURI (string, optional): The path to the individual story resource.,
    var name: String? // name (string, optional): The canonical name of the story.,
    var type: String? // type (string, optional): The type of the story (interior or cover).
}
