//
//  ComicDataWrapper.swift
//  project616
//
//  Created by Adrian Eves on 5/22/22.
//

import Foundation

struct ComicDataWrapper: Codable {
    var code: Int? //code (int, optional): The HTTP status code of the returned result.,
    var status: String? //status (string, optional): A string description of the call status.,
    var copyright: String? //copyright (string, optional): The copyright notice for the returned result.,
    var attributionText: String? //attributionText (string, optional): The attribution notice for this result. Please display either this notice or the contents of the attributionHTML field on all screens which contain data from the Marvel Comics API.,
    var attributionHTML: String? //attributionHTML (string, optional): An HTML representation of the attribution notice for this result. Please display either this notice or the contents of the attributionText field on all screens which contain data from the Marvel Comics API.,
    var data: ComicDataContainer? //data (ComicDataContainer, optional): The results returned by the call.,
    var etag: String? //etag (string, optional): A digest value of the content returned by the call.
}
