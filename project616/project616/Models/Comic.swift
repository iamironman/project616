//
//  Comic.swift
//  project616
//
//  Created by Adrian Eves on 5/22/22.
//

import Foundation

struct Comic: Codable {
    var id: Int? //id (int, optional): The unique ID of the comic resource.,
    var digitalId: Int? // digitalId (int, optional): The ID of the digital comic representation of this comic. Will be 0 if the comic is not available digitally.,
    var title: String? //title (string, optional): The canonical title of the comic.,
    var issueNumber: Double? //issueNumber (double, optional): The number of the issue in the series (will generally be 0 for collection formats).,
    var variantDescription: String? //variantDescription (string, optional): If the issue is a variant (e.g. an alternate cover, second printing, or director’s cut), a text description of the variant.,
    var description: String? //description (string, optional): The preferred description of the comic.,
    var modified: String? // modified (Date, optional): The date the resource was most recently modified.,
    var isbn: String? //isbn (string, optional): The ISBN for the comic (generally only populated for collection formats).,
    var upc: String? //upc (string, optional): The UPC barcode number for the comic (generally only populated for periodical formats).,
    var diamondCode: String? //diamondCode (string, optional): The Diamond code for the comic.,
    var ean: String? //ean (string, optional): The EAN barcode for the comic.,
    var issn: String? //issn (string, optional): The ISSN barcode for the comic.,
    var format: String? //format (string, optional): The publication format of the comic e.g. comic, hardcover, trade paperback.,
    var pageCount: Int? //pageCount (int, optional): The number of story pages in the comic.,
    var textObjects: [TextObject]? //textObjects (Array[TextObject], optional): A set of descriptive text blurbs for the comic.,
    var resourceURI: String? //resourceURI (string, optional): The canonical URL identifier for this resource.,
    var urls: [ExternalURL]? // urls (Array[Url], optional): A set of public web site URLs for the resource.,
    var series: SeriesSummary? //series (SeriesSummary, optional): A summary representation of the series to which this comic belongs.,
    var variants: [ComicSummary]? //variants (Array[ComicSummary], optional): A list of variant issues for this comic (includes the "original" issue if the current issue is a variant).,
    var collections: [ComicSummary]? //collections (Array[ComicSummary], optional): A list of collections which include this comic (will generally be empty if the comic's format is a collection).,
    var collectedIssues: [ComicSummary]? //collectedIssues (Array[ComicSummary], optional): A list of issues collected in this comic (will generally be empty for periodical formats such as "comic" or "magazine").,
    var dates: [ComicDate]? //dates (Array[ComicDate], optional): A list of key dates for this comic.,
    var prices: [ComicPrice]? //prices (Array[ComicPrice], optional): A list of prices for this comic.,
    var thumbnail: Thumbnail? // thumbnail (Image, optional): The representative image for this comic.,
    var images: [ExternalImage]? //images (Array[Image], optional): A list of promotional images associated with this comic.,
    var creators: CreatorList? //creators (CreatorList, optional): A resource list containing the creators associated with this comic.,
    var characters: CharacterList? //characters (CharacterList, optional): A resource list containing the characters which appear in this comic.,
    var stories: StoryList? //stories (StoryList, optional): A resource list containing the stories which appear in this comic.,
    var events: EventList? // events (EventList, optional): A resource list containing the events in which this comic appears.
    
    
    // CUSTOM FUNCTIONS
    func assembleThumbnailUrlString() -> String {
        guard var thumbnailPath = self.thumbnail?.path else {
            return ""
        }
        
        if thumbnailPath.prefix(5) != "https" {
            let index = thumbnailPath.index(thumbnailPath.startIndex, offsetBy: 4)
            thumbnailPath.insert("s", at: index)
        }
        
        guard let thumbnailExtension = self.thumbnail?.extension else {
            return ""
        }
        
        return "\(thumbnailPath).\(thumbnailExtension)"
    }
    
    func accessibilityAltImageText() -> String {
        var labelString = "A cover of a Marvel Comic book"
        var descriptionCollection: [String] = []
        // Things to consider:
        // 1: Is this a variant cover?
        // 2: What characters appear in this?
        // 3: Is this part of a series?
        // 4: Is this part of a collection?
        // 5: Who are the creators?
        // 6: Is this part of an event?
        
        if let variantDescription = self.variantDescription {
            if variantDescription != "" {
                descriptionCollection.append(variantDescription)
            }
        }
        
        if let characters = self.characters?.items {
            var characterList = [String]()
            for character in characters {
                if let characterName = character.name {
                    characterList.append(characterName)
                }
            }
            if !characterList.isEmpty {
                let prefix = "Characters in this issue include "
                //let list = characterList.joined(separator: ", ")
                let list = characterList.formatted(.list(type: .and))
                descriptionCollection.append("\(prefix + list).")
            }
        }
        
        if let seriesName = self.series?.name {
            let prefix = "This issue belongs to a series called "
            descriptionCollection.append("\(prefix + seriesName).")
        }
        
        if let collection = self.collections {
            var collectionNames = [String]()
            for summary in collection {
                if let name = summary.name {
                    collectionNames.append(name)
                }
            }
            if !collectionNames.isEmpty {
                let prefix = "Part of collection set "
                //let list = collectionNames.joined(separator: ",")
                let list = collectionNames.formatted(.list(type: .and))
                descriptionCollection.append("\(prefix + list).")
            }
        }
        
        if let creators = self.creators?.items {
            var creatorsList = [String]()
            for creator in creators {
                if let creatorName = creator.name {
                    creatorsList.append(creatorName)
                }
            }
            if !creatorsList.isEmpty {
                let prefix = "Created by "
                //let list = creatorsList.joined(separator: ", ")
                let list = creatorsList.formatted(.list(type: .and))
                descriptionCollection.append("\(prefix + list).")
            }
        }
        
        if let events = self.events?.items {
            var eventsList = [String]()
            for event in events {
                if let eventName = event.name {
                    eventsList.append(eventName)
                }
            }
            if !eventsList.isEmpty {
                let prefix = "Part of the event set "
                //let list = eventsList.joined(separator: ", ")
                let list = eventsList.formatted(.list(type: .and))
                descriptionCollection.append("\(prefix + list).")
            }
        }
        
        if !descriptionCollection.isEmpty {
            labelString = descriptionCollection.joined(separator: " ")
        }
        
        return labelString
    }    
}
