//
//  ComicDataContainer.swift
//  project616
//
//  Created by Adrian Eves on 5/22/22.
//

import Foundation

struct ComicDataContainer: Codable {
    var offset: Int? //offset (int, optional): The requested offset (number of skipped results) of the call.,
    var limit: Int? //limit (int, optional): The requested result limit.,
    var total: Int? //total (int, optional): The total number of resources available given the current filter set.,
    var count: Int? //count (int, optional): The total number of results returned by this call.,
    var results: [Comic]? //results (Array[Comic], optional): The list of comics returned by the call
}
