//
//  Thumbnail.swift
//  project616
//
//  Created by Adrian Eves on 5/22/22.
//

import Foundation

struct Thumbnail: Codable {
    var path: String?
    var `extension`: String?
}
