//
//  ComicSummary.swift
//  project616
//
//  Created by Adrian Eves on 5/22/22.
//

import Foundation

struct ComicSummary: Codable {
    var resourceURI: String? // resourceURI (string, optional): The path to the individual comic resource.,
    var name: String? // name (string, optional): The canonical name of the comic.
}
