//
//  ComicCollectionViewCell.swift
//  project616
//
//  Created by Adrian Eves on 5/22/22.
//

import SwiftUI

struct ComicCollectionViewCell: View {
    @Binding var showingDetail: Bool
    @Binding var comicSelection: Comic?
    var comic: Comic
    var comicTitle: String {
        return comic.title ?? "Title Unavailable"
    }
    
    var body: some View {
        HStack {
            Text(comicTitle)
                .font(.headline)
            Spacer()
        }
        .contentShape(Rectangle())
        
        .onTapGesture {
            comicSelection = comic
            showingDetail = true
        }
        .accessibilityLabel(Text(comicTitle))
        .accessibilityHint(Text("Activate to view more information about this issue"))
        .accessibilityAddTraits(.isButton)
    }
}

//struct ComicCollectionViewCell_Previews: PreviewProvider {
//    static var previews: some View {
//        ComicCollectionViewCell()
//    }
//}
