//
//  ComicDetailView.swift
//  project616
//
//  Created by Adrian Eves on 5/22/22.
//

import SwiftUI

struct ComicDetailView: View {
    @Environment(\.dismiss) var dismiss
    
    var comic: Comic?
    var title: String {
        return comic?.title ?? "No Title Available"
    }
    var description: String {
        if let availableDescription = comic?.description {
            return availableDescription
        }
        
        return comic?.accessibilityAltImageText() ?? "No description available"
    }
    
    var body: some View {
        NavigationView {
            VStack {
                AsyncImage(url: URL(string: comic?.assembleThumbnailUrlString() ?? "https://www.marvel.com")) { image in
                    image.resizable()
                        .accessibilityLabel(Text(comic?.accessibilityAltImageText() ?? "Cover unavailable"))
                } placeholder: {
                    Image(uiImage: UIImage(named: "No_Image_Cover")!)
                        .resizable()
                }
                .scaledToFit()
                .frame(height: 256)
                .accessibilityAddTraits(.isButton)
                Text(title)
                    .font(.title)
                    .fontWeight(.bold)
                    .padding()
                Text(description)
                    .accessibilityAddTraits(.isStaticText)
                    .font(.subheadline)
                    .padding([.leading, .trailing, .bottom])
            }
            .navigationBarItems(trailing: Button {
                dismiss()
            } label: {
                Text("Done")
            }
                .accessibilityLabel("Dismiss comic")
                .accessibilityAddTraits(.isButton)
            )
        }
    }
}

//struct ComicDetailView_Previews: PreviewProvider {
//    static var previews: some View {
//        ComicDetailView()
//    }
//}
