//
//  ComicCollectionListView.swift
//  project616
//
//  Created by Adrian Eves on 5/22/22.
//

import SwiftUI

struct ComicCollectionListView: View {
    @StateObject var comicCollectionViewModel = ComicCollectionViewModel()
    @State var showDetail = false
    
    var body: some View {
        VStack {
            switch comicCollectionViewModel.contentStatus {
            case .available:
                NavigationView {
                    List {
                        ForEach(comicCollectionViewModel.networkManager.results, id: \.self.id) { comic in
                            ComicCollectionViewCell(showingDetail: $showDetail, comicSelection: $comicCollectionViewModel.selectedComic, comic: comic)
                        }
                    }
                    .accessibilityIdentifier("comicListView")
                    .navigationTitle(Text("Adrian's Collection"))
                }
            case .unavailable:
                VStack {
                Image(uiImage: UIImage(named: "spiderdefeat")!)
                        .resizable()
                        .scaledToFit()
                        .accessibilityLabel("Spider-Man has fallen to his knees and raises his hands in the air in frustration because Thor's hammer has been placed on top of the toilet seat.")
                        .accessibilityAddTraits(.isImage)
                Text("No internet connection, True Believers.")
                    .accessibilityIdentifier("comicListUnavailableView")
                }
            case .loading:
                VStack {
                    Image(uiImage: UIImage(named: "Marvel_Logo")!)
                        .resizable()
                        .scaledToFit()
                        .accessibilityIdentifier("MarvelLogoLoadingImage")
                        .accessibilityLabel("Marvel logo")
                    Text(LoadingString.randomString())
                        .padding(.bottom)
                        .accessibilityIdentifier("LoadingString")
                    ProgressView()
                        .scaleEffect(3)
                        .accessibilityIdentifier("LoadingProgressView")
                }
                .accessibilityElement(children: .combine)
            }
        }
        .sheet(isPresented: $showDetail) {
            ComicDetailView(comic: comicCollectionViewModel.selectedComic)
        }
    }
}


//struct ComicCollectionListView_Previews: PreviewProvider {
//    static var previews: some View {
//        ComicCollectionListView()
//    }
//}
