//
//  project616Tests.swift
//  project616Tests
//
//  Created by Adrian Eves on 5/22/22.
//

import XCTest
@testable import project616

class project616Tests: XCTestCase {    
    
    func test_network_manager_loads_correct_url() {
        // Test to see if we're experiencing success with the correct URL.
        let urlString = MarvelURL.urlString()
        let url = URL(string: urlString) ?? URL(string: "https://www.marvel.com/")!
        let manager = NetworkManager(url: url)
        let session = URLSessionMock()
        let expectation = XCTestExpectation(description: "Downloading comics.")
        
        manager.loadComicCollectionTest(using: session) {
            XCTAssertEqual(URL(string: urlString), session.lastURL)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }
    
    func test_network_manager_calls_resume() {
        // Test to see if we can successfully resume
        let url = URL(string: MarvelURL.urlString()) ?? URL(string: "https://www.marvel.com")!
        let manager = NetworkManager(url: url)
        let session = URLSessionMock()
        let expectation = XCTestExpectation(description: "Retrieving comics calls resume().")
        manager.loadComicCollectionTest(using: session) {
            XCTAssertTrue(session.dataTask?.resumeWasCalled ?? false)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }
    
    func test_accessibility_alt_image_text_empty_comic() {
        let testComic = Comic(id: 1)
        
        // Empty Comic
        XCTAssertEqual(testComic.accessibilityAltImageText(), "A cover of a Marvel Comic book")
    }
    
    func test_accessibility_alt_image_text_fetched_comic() {
        // We're going to test for
        // 1: Fetching comics successfully and
        // 2: Generating proper alt text
        let manager = NetworkManager(url: URL(string: MarvelURL.urlString())!)
        let session = URLSession.shared
        let expectationFetch = XCTestExpectation(description: "Fetching comic")
        let expectationAltText = XCTestExpectation(description: "Applying alt text")
        manager.loadComicCollectionTest(using: session) {
            sleep(2) // Allowing the manager to collect the comics
            let results = manager.results
            XCTAssert(!results.isEmpty)
            expectationFetch.fulfill()
            
            let altText = results.first?.accessibilityAltImageText()
            XCTAssert(altText != nil)
            if let altText = altText {
                XCTAssert(!altText.isEmpty)
            } else {
                XCTFail("Alt text returned an empty string!")
            }
            expectationAltText.fulfill()
        }
        // Fetched Comic and Alt Text!
        wait(for: [expectationFetch, expectationAltText], timeout: 5)
    }
}
