//
//  URLSessionMock.swift
//  project616
//
//  Created by Adrian Eves on 5/23/22.
//

import Foundation

protocol URLSessionProtocol {
    func dataTask(with url: URL, completionHandler: @escaping
    (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask
}

extension URLSession: URLSessionProtocol { }

class URLSessionMock: URLSessionProtocol {
    var lastURL: URL?
    var dataTask: DataTaskMock?
    var testData: Data?
    
    func dataTask(with url: URL, completionHandler: @escaping
    (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        let newDataTask = DataTaskMock(completionHandler: completionHandler)
        dataTask = newDataTask
        lastURL = url
        return newDataTask
    }
}

class DataTaskMock: URLSessionDataTask {
    var completionHandler: (Data?, URLResponse?, Error?) -> Void
    var resumeWasCalled = false
    init(completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {
        self.completionHandler = completionHandler
    }
    
    override func resume() {
        resumeWasCalled = true
        completionHandler(nil, nil, nil)
    }
}
