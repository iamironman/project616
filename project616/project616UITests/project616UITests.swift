//
//  project616UITests.swift
//  project616UITests
//
//  Created by Adrian Eves on 5/22/22.
//

import XCTest

class project616UITests: XCTestCase {
    
    // There is a bug in my current build of Xcode
    // that prevents me from creating a version of
    // the app variable on the instance scope.
    // I have manually added the app variable before
    // each test set in the meantime.
    
    func test_comic_view_has_loaded() {
        let app = XCUIApplication()
        app.launch()
        
        let loadedNavigationBarText = app.staticTexts["Adrian's Collection"]
        XCTAssert(loadedNavigationBarText.waitForExistence(timeout: 2))
        
        let comicTable = app.collectionViews["comicListView"]
        XCTAssert(comicTable.waitForExistence(timeout: 2))
    }
    
    func test_select_item_from_comics_view() {
        let app = XCUIApplication()
        app.launch()
        app.collectionViews["comicListView"].buttons.firstMatch.tap()
        
        // We're trying to see if we have two
        // static text areas for the title
        // and description.
        sleep(1)
        // The static text "Adrian's Collection"
        // is still present due to the nature of
        // Sheet presentation.
        XCTAssert(app.staticTexts.count == 3)
    }
    
    func test_dismiss_sheet_button() {
        let app = XCUIApplication()
        app.launch()
        app.collectionViews["comicListView"].buttons.firstMatch.tap()
        
        app.buttons.firstMatch.tap()
        
        // Static text should be 1
        XCTAssert(app.staticTexts.count == 1)
        
        // Button count should be greater than 1
        XCTAssert(app.buttons.count >= 1)
    }
}
