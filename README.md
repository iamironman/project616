#  project616: Adrian's Collection

This project was really fun to work on. Truth be told, I had a harder time telling myself "no" to throwing additional things into the mix. But we're not necessarily here to listen to me ramble on about excitement and nerd alerts (unless you are, in which case I'm always up for that). Let's go over what's included in this project:

- Several SwiftUI views
- A NetworkManager class
- A special MarvelURL class
- Some mock classes
- A view model
- Unit Tests
- UI Tests
- A Keys config file

This project is designed to be able to run on device or in the simulator right out of the box. I designed it for the iPod touch (Rest In Peace, old friend) simply because it's the UI killer for most apps, and I wanted to avoid such pain points.

I'll go ahead and explain what you can expect. I want to start with the elephant in the room, the Keys config file. I know what you're thinking--what's the point of using secret keys if I'm just going to make them available in the repository? Great question, but simply put, I've included it for the sake of compilation and testing. I know, in the real world, I should apply .gitignore rules to the config files like that, but since you're kind enough to review my code, I wanted to make it easy!

Next up are my SwiftUI views. Given more time, I might have tried to get fancier with the UI, but the provided layouts are not only clean but highly accessible. This app conditionally presents one of three views based on loading progress, and depending on how your internet is working, you'll get a screen when content is loading, one for loaded material, and one for when things just didn't go according to plan. The real deal here is the view that presents when the content has successfully loaded. It's a CollectionView of comic book titles, and selecting them issues a sheet that displays some information on the book. I realize the assignment said to display at least one comic, but I got greedy and did up to 20. And you know what? After I submit this for review, I'll probably start reading a few of them.

My SwiftUI views are all dependent on a view model that controls the state of what's shown. I don't like doing model or network operations on a view because, honestly, it feels messy and I'm afraid the MVVM police will come after me.

The view model owns a NetworkManager object, which does the real work, thanks to observations and a sprinkling of Combine subscriptions. Basically, it's in this file that I run my network call and change the loading state so the view model can then make decisions based on that. I made a quick and easy class called MarvelURL that exists to serve up a url that'll fetch comic data. It's mostly a static string with a bit of interpolated hashing, but it gets the job done.

I have unit tests and UI tests that all pass at the time of submission, and they're not terribly wild since it's a pretty tame app.

Lastly, I want to mention that I took a significant amount of time to monitor this app's accessibility. I think accessibility is extremely important no matter what you're making, and I feel that even a sample application should be made available for anyone, regardless of their background. While most of my additions are made for visual assistance, the choice to build this in SwiftUI means that we get a lot of additional accessibility control for free, especially for Switch Control users. I actually threw together an altText method for comic book covers, and it worked well enough that, when I saw my comic issue descriptions from my GET requests were listed as null on the backend, I used some nil coalescing to add this description instead. It worked out extremely well.

Anyways, thanks for taking a look at this, and even better, thank you for assigning it to me. I haven't been able to use SwiftUI--or hardly Swift itself, for that matter--in a year, and this was just a blast.

Excelsior!

